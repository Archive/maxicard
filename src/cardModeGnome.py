# Maxi Card
# Copyright (C) 2000 Free Software Foundation.
#
# Author: OFSET, Hilaire Fernandes
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the  Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
# USA.
#

##################################################
#                                                # 
# Implementations of the xxxModeUI class         #
#                                                #
##################################################

import popen2, os, re, string
from gtk import *
from virtualClassUI import cardModeUI
from cardMode import browseModeUI, browseSelectionModeUI, \
     insertModeUI, editModeUI, findModeUI
from gnomeTools import setBuffer

class browseModeGnome(browseModeUI):
    def __init__ (self, ui):
        self.ui = ui
        self.ui.widgets["ok"].hide ()
        self.ui.widgets["keepCard"].set_active (0)
        self.ui.widgets["previewLatexVpaned"].set_position (8000)
        self.ui.widgets["barre"].set_default ("Maxi Card - Browsing the card")
    def __del__ (self):
        self.ui.widgets["ok"].show ()
        self.ui.widgets["previewLatexVpaned"].set_position (-1)

class browseSelectionMode(browseSelectionModeUI):
    pass

class insertModeGnome(insertModeUI):
    def __init__ (self, ui, categoryID, treeItem):
        insertModeUI.__init__ (self, ui, categoryID)
        self.treeItem = treeItem
        self.ui.widgets["next"].hide ()
        self.ui.widgets["previous"].hide ()
        self.ui.widgets["keepCard"].hide ()
        setBuffer (self.ui.widgets["latexText"], "")
        self.ui.widgets["latexText"].set_editable (1)
        setBuffer (self.ui.widgets["commentText"], "")
        self.ui.widgets["commentText"].set_editable (1)
        # empty the entry
        self.ui.widgets["level1Entry"].set_text ("")
        self.ui.widgets["difficulty1Entry"].set_text ("")
        self.ui.widgets["level2Entry"].set_text ("")
        self.ui.widgets["difficulty2Entry"].set_text ("")
        self.ui.widgets["level3Entry"].set_text ("")
        self.ui.widgets["difficulty3Entry"].set_text ("")
        self.ui.widgets["keywordEntry"].set_text ("")
        self.ui.widgets["contributorEntry"].set_text ("")
        self.ui.widgets["emailEntry"].set_text ("")
        self.ui.widgets["previewLatexVpaned"].set_position (0)
        self.ui.widgets["barre"].set_default ("Maxi Card - Inserting a new card")

        self.ui.widgets["IDEntry"].set_text (self.categoryID + "/" + str(self.id))
        self.ui.widgets["IDEntry"].set_editable (0)
 
    def __del__ (self):
        insertModeUI.__del__ (self)        
        self.ui.widgets["next"].show ()
        self.ui.widgets["previous"].show ()
        self.ui.widgets["keepCard"].show ()
        self.ui.widgets["latexText"].set_editable (0)
        self.ui.widgets["commentText"].set_editable (0)
        self.ui.widgets["previewLatexVpaned"].set_position (-1)

    def okPressed (self):
        self.ui.widgets["main"].ok_cancel ("I will now add the card informations in the data base.\nThen you must copy the card file and its relatives files, in the card location.\nThe card files are composed of one mandatory card.tex file plus optional .tex and .eps files.\nTo help you, I will open the folder where to copy the card files.", self.cb_copyCardFile)

    def cb_copyCardFile (self, cancel):
        # cancel = 1 when Cancel and 0 when Ok
        if not cancel:
           # create the category folder
            cardPath = self.ui.DBPath + "/" \
                       + self.ui.widgets["IDEntry"].get_text ()
            # start GMC on the folder location
            os.makedirs (cardPath)
            pid = os.fork ()
            if pid == 0:
                os.execl ("/usr/bin/gnome-terminal","gnome-terminal", "-e", "mc "+'"'+cardPath+'"')
                os._exit ()
            elif pid > 0:
                self.childsPid.append (pid)
                # Append the card in the UI in the Data base
                self.ui.DataBase.appendCard (self.ui.fetchCard ())
                item = GtkTreeItem (str(self.id))
                self.treeItem.append (item)
                item.show ()
                item ["name"] =  self.ui.widgets["IDEntry"].get_text ()
                item.connect ("select", self.ui.cb_select_card, item["name"])
class editModeGnome(editModeUI):
    pass

class findModeGnome(findModeUI):
    pass
