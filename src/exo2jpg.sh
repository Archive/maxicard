#!/bin/sh

ExoBasePath=$1
exo=$2
enteteSpecif=$3
exoPath=$ExoBasePath/$exo
exo=$exo/card.tex

cd $ExoBasePath
# Mettre ces ent�tes par d�faut
echo "\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}
$enteteSpecif
\makeatletter
\pagestyle{empty}

\begin{document}" | cat - "$exo" > "$exoPath/compile.tex"

echo "\end{document}" >> "$exoPath/compile.tex"

# Compiler le code tex en dvi
cd "$exoPath"

latex --interaction nonstopmode "compile.tex"

# Convertir en ps
dvips -f "compile.dvi" > "compile.ps"

# Convertir en jpg
convert -depth 8 -antialias -crop 0x0+4+4 -density 96 \
    compile.ps card.jpg

mv card.jpg.0 card.jpg

# Cleaning...
rm card.jpg.?
rm -f compile.*
rm -f *~
