# Maxi Card
# Copyright (C) 2000 OFSET & the Free Software Foundation.
#
# Author: Hilaire Fernandes
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the  Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
# USA.
#
import re
import string
class card:
    def __init__ (self, stringCard = ""):
        "Build a card new (empty) or from a string representation"
        if stringCard == "":
            self.card = {"id": "",
                         "level1" : "",
                         "difficulty1" : "",
                         "level2" : "",
                         "difficulty2" : "",
                         "level3" : "",
                         "difficulty3" : "",
                         "keywords": "",
                         "comments" : "",
                         "LaTeXHeader" :  "",
                         "source" : "",
                         "sourceEmail" : ""}
        else:
            #fetch form the string
            result = string.split (stringCard, "\t")
            self.card = {"id" : result[0],
                         "level1" : result[1],
                         "difficulty1" : result[2],
                         "level2" : result[3],
                         "difficulty2" : result[4],
                         "level3" : result[5],
                         "difficulty3" : result[6],
                         "keywords" : result[7],
                         "comments" : result[8],
                         "LaTeXHeader" : result[9],
                         "source" :  result[10],
                         "sourceEmail" : result[11]}
            #Turn ^K as \n caracteres
            self.card["comments"] = re.sub (r"\013", "\n", self.card["comments"])
            self.card["LaTeXHeader"] = re.sub (r"\013","\n",
                                               self.card["LaTeXHeader"])
    def __del__ (self):
        "Any cleaning?"
        pass
    def getStringCard (self):
        "Return a string representation of the card, suitable to the database"
        result = self.card["id"] + "\t"
        result = result + self.card["level1"] + "\t" + self.card["difficulty1"] + "\t"
        result = result + self.card["level2"] + "\t" + self.card["difficulty2"] + "\t"
        result = result + self.card["level3"] + "\t" + self.card["difficulty3"] + "\t"
        result = result + self.card["keywords"] + "\t" + self.card["comments"] + "\t" \
                 + self.card["LaTeXHeader"] + "\t"
        result = result + self.card["source"] + "\t" + self.card["sourceEmail"]
        result = re.sub ("\n", r"\013", result)
        return result
    def getCard (self):
        "Return the entire card in a dictionnary"
        return self.card
    def setCardItem (self, key, value):
        "Set item value using the key of the card dictionnary"
        self.card[key]=value
