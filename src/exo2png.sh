#!/bin/sh

# $1 l'emplacement de l'exercice comme dans la base
# pwd | sed 's/\(.*\)\(ExoBase\/\)\(.*\)/\3/'

ExoBasePath=/home/hilaire/Scolaire/ExoBase
DB=ExoBase.db


exo=$1 

# Le numero d'ID de l'exo dans la base
exoPath=`expr "$1" : '\(.*\)\/.*'`

# Cherche la fiche de l'exercice dans la base de donn�e
fiche=`grep "$exoPath" $ExoBasePath/ExoBase.db`

# Optenir le caract�re de contr�le ^K 
ck=`echo -e '\013'`
# D�terminer les ent�tes sp�cifique � l'exercice
enteteSpecif=`echo  "$fiche" | cut - -f10`

# Mettre ces ent�tes par d�faut
echo "\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}
$enteteSpecif
\makeatletter
\pagestyle{empty}

\begin{document}" | cat - "$exo" > "$exoPath/compile.tex"

echo "\end{document}" >> "$exoPath/compile.tex"

# Remplacer les caracteres de controle ^K par \n [note: dans forms ^K
# siginifie une fin de ligne dans un champs]
sed "s/$ck"'/\
/g' "$exoPath/compile.tex" | cat - > "$exoPath/compile.tex"

# Compiler le code tex en dvi
cd "$exoPath"

latex "compile.tex"

# Convertir en ps
dvips -f "compile.dvi" > "compile.ps"

# Convertir en png
convert -antialias -crop 0x0+4+4 -density 96 \
    compile.ps exercice.jpg

# Cleaning...
rm -f compile.*
rm -f *~