# Maxi Card
# Copyright (C) 2000 Free Software Foundation.
#
# Author: OFSET, Hilaire Fernandes
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the  Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
# USA.
#

##################################################
#                                                # 
# Implementations of the cardModeUI class        #
#                                                #
##################################################

import string, os, re
from virtualClassUI import cardModeUI

class browseModeUI(cardModeUI):
    "The default mode, the next and previous, keepit button are present"
    def keepItPressed (self, active):
        if active:
            self.ui.keepCard (self.ui.selectedCard)
        else:
            self.ui.unkeepCard (self.ui.selectedCard)
    def nextPressed (self):
        self.ui.pushCard (self.ui.DataBase.getNextCard ())
    def previousPressed (self):
        self.ui.pushCard (self.ui.DataBase.getPreviousCard ())    

class browseSelectionModeUI(cardModeUI):
    "To browse a selected list of card. The selection can be from \
    a search and a hand selection using the keep it button"
    pass

class insertModeUI(cardModeUI):
    "To insert a new card in the database"
    def __init__ (self, ui, categoryID):
        # keep track of the child process created in this mode
        self.childsPid = []
        self.ui = ui
        self.categoryID = categoryID
        self.id = 1
        # Set an automatic ID of the card before check the provided category first
        # Does the user select a card instead of a category ?        
        self.categoryID = self.ui.forceToCategory (self.categoryID)
        categoryPath = self.ui.DBPath + "/" + self.categoryID
        directory = os.listdir (categoryPath)
        while str(self.id) in directory:
            self.id = self.id + 1
    def __del__ (self):
        # kill any running subprocess
        for pid in self.childsPid:
            os.kill (pid, 9)                

class editModeUI(cardModeUI):
    "To edit a card"
    pass

class findModeUI:
    "To research a card"
