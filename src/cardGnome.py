# Maxi Card
# Copyright (C) 2000 Free Software Foundation.
#
# Author: OFSET, Hilaire Fernandes
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the  Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
# USA.
#

import os, os.path, re, string
from virtualClassUI import cardUI
from DBCard import DBCard
from card import card
from cardModeGnome import browseModeGnome, browseSelectionMode, \
     insertModeGnome, editModeGnome, findModeGnome
from preferenceGnome import preferenceGnome
from gnomeTools import setBuffer

from gtk import *
from gnome.ui import *
from GDK import *
from libglade import *

class cardGnome(cardUI):
    'Gnome implementation of the card interface'
    def __init__ (self, filename, DBPath, database):
        "Load the Glade interface plus other initialisation"
        # child process reference for conversion sub process
        self.child_process = None
        self.guiXML = filename
        self.glade = GladeXML (filename, "maxicard")
        # fetch some widget reference from glade
        self.widgets = {
            'main' :self.glade.get_widget("maxicard"),
            'barre' : self.glade.get_widget("maxicardBar"),
            "cardTree" : self.glade.get_widget ("cardTree"),
            "IDEntry" : self.glade.get_widget ("IDEntry"),
            "level1Entry" : self.glade.get_widget ("level1Entry"),
            "difficulty1Entry" : self.glade.get_widget ("difficulty1Entry"),
            "level2Entry" : self.glade.get_widget ("level2Entry"),
            "difficulty2Entry" : self.glade.get_widget ("difficulty2Entry"),
            "level3Entry" : self.glade.get_widget ("level3Entry"),
            "difficulty3Entry" : self.glade.get_widget ("difficulty3Entry"),
            "keywordEntry" : self.glade.get_widget ("keywordEntry"),
            "commentText" : self.glade.get_widget ("commentText"),
            "contributorEntry" : self.glade.get_widget ("contributorEntry"),
            "emailEntry" : self.glade.get_widget ("emailEntry"),
            "pixmapPreview" : self.glade.get_widget ("pixmapPreview"),
            "latexText" : self.glade.get_widget ("latexText"),
            "next" : self.glade.get_widget ("next"),
            "previous" : self.glade.get_widget ("previous"),
            "ok" : self.glade.get_widget ("ok"),
            "keepCard" : self.glade.get_widget ("keepCard"),
            "previewLatexVpaned" : self.glade.get_widget ("previewLatexVpaned")
            }
        
        
        dic = {"on_exit2_activate": mainquit,
               "on_next_clicked": self.cb_next_card,
               "on_previous_clicked" : self.cb_previous_card,
               "on_ok_clicked" : self.cb_ok_clicked,
               "on_keepCard_toggled" : self.cb_keepCard_toggled,
               "on_newCategory_clicked" : self.cb_newCategory_clicked,
               "on_newCard_clicked" : self.cb_newCard_clicked,
               "on_browseCard_clicked" : self.cb_browseCard_clicked,
               "on_deleteCard_clicked" : self.cb_deleteCard_clicked,
               "on_deleteCategory_clicked" : self.cb_deleteCategory_clicked,
               "on_about2_activate" : self.cb_showAbout,
               "on_preferences2_activate" : self.cb_setPreferences
               }
        self.glade.signal_autoconnect (dic)

        progress = self.widgets["barre"].get_progress ()
        self.widgets["progress"] = progress
        self.initCardGnome (DBPath, database)
        

    def initCardGnome (self, DBPath, database):
        # init the base class which in turn init the database
        cardUI.__init__ (self, DBPath, database)
        if self.DataBase.state == 'unvalid':
            self.widgets["main"].error ("The database specified is not valid.")
            # We should ask if user want ot create a new one
            # or revert to the previous one

        # generate the item tree
        self.widgets["barre"].push("Generating categories tree")
        self.delTree ()
        self.buildTree (self.widgets["cardTree"], DBPath)
        self.widgets["barre"].pop ()
        self.widgets["progress"].set_activity_mode (TRUE)
        
        # set to the default mode
        self.mode = browseModeGnome (self)

    def cb_select_card (self, treeItem, cardID):
        "Callback when tree item (only exercice) are selected \
        The cardID is the first record in the database"
        # Create the corresponding object
        self.pushCard (self.DataBase.getCard (cardID))
    def cb_next_card (self, w):
         self.mode.nextPressed ()
    def cb_previous_card (self, w):
         self.mode.previousPressed ()
    def cb_ok_clicked (self, w):
        self.mode.okPressed ()
    def cb_keepCard_toggled (self, w):
        self.mode.keepItPressed (w["active"])
    def cb_newCategory_clicked (self, w):
        # Determine the parent tree Item and the parent path name
        if  self.widgets["cardTree"].get_selection () == []:
            name = ""
            treeItem = self.widgets["cardTree"]
        else:
            treeItem =  self.widgets["cardTree"].get_selection ()[0]
            name = treeItem["name"]
            if name != self.forceToCategory (name):
                # Not a category
                self.widgets["main"].error ("You must select a category not a card.")
                return
            treeItem = treeItem.subtree
        # FIXME: This is very bad
        self.widgets["main"].set_data ("newCategory", name)
        self.widgets["main"].set_data ("parentTreeItem", treeItem)
        
        self.widgets["main"].request_string ("Enter the new category name in "+name+": ",
                                            self.cb_newCategoryString)
        
    def cb_newCategoryString (self, categories):
        if categories != None:
            # FIXME: fetch the newCategory and parentTreeItem by a safer way
            # This is terribly uggly and doesn't work if the user
            # do some thing else as the dialog is not modal            
            os.makedirs (self.DBPath+"/"+self.widgets["main"].get_data ("newCategory")+"/"+categories)            
            # Detect if there is several slash in the category name, so split in sub tree
            start = 0
            categories = categories + "/"
            pos = string.find (categories, "/", start)
            tree = self.widgets["main"].get_data ("parentTreeItem")
            while pos <> -1:
                item = GtkTreeItem (categories[start:pos])
                tree.append (item)
                item.show ()
                item["name"] = self.widgets["main"].get_data ("newCategory")+"/"+categories[start:pos]
                item.connect ("select", self.cb_select_card, item["name"])
                # Turn the category as a sub tree
                tree = GtkTree ()
                item.set_subtree (tree)
                start = pos + 1
                pos = string.find (categories, "/", start)
                                              
    def cb_newCard_clicked (self, w):
        # Get the selected category
        if  self.widgets["cardTree"].get_selection () == []:
            self.widgets["main"].error ("First, select a category on the tree at the left.\nIf the category you want doesn't exist create it first.")
        else:
            treeItem =  self.widgets["cardTree"].get_selection ()[0]
            name = treeItem["name"]
            if name != self.forceToCategory (treeItem["name"]):
                # Not a category
                self.widgets["main"].error ("You must select a category not a card.")
                return
            del self.mode
            self.mode = insertModeGnome (self, name, treeItem.subtree)
    def cb_browseCard_clicked (self, w):
        del self.mode
        self.mode = browseModeGnome (self)
    def cb_deleteCard_clicked (self, w):
        if self.selectedCard != None:
            tree = self.searchCardItemInTree (self.widgets["cardTree"], self.selectedCard.card["id"])
            if tree != None: tree.destroy ()
            self.DataBase.delCard (self.selectedCard)
            # remove the card files
            try:
                os.removedirs (self.DBPath+"/"+self.selectedCard.card["id"])
            except OSError:
                pass
            del self.selectedCard
            self.pushCard (self.DataBase.getCurrentCard ())
    def cb_deleteCategory_clicked (self, w):
        if  self.widgets["cardTree"].get_selection () != []:
            treeItem =  self.widgets["cardTree"].get_selection ()[0]
            name = treeItem["name"]
            if name != self.forceToCategory (name):
                # Not a category
                self.widgets["main"].error ("You must select a category not a card.")
                return
            else:
                self.widgets["main"].ok_cancel ("Confirm you want to remove category\n"+name+"\nand its content.",
                                                self.cb_confirmRemoveCategory)
    def cb_showAbout (self, w):
        GladeXML (self.guiXML, "maxicardAbout").get_widget ("maxicardAbout").show ()

    def cb_setPreferences (self, w):
        self.preferenceUI = preferenceGnome (self.guiXML, self, self.DBPath, self.databaseName)

    def cb_confirmRemoveCategory (self, cancel):
        # cancel = 1 when Cancel and 0 when ok
        if cancel == 0:
            treeItem =  self.widgets["cardTree"].get_selection ()[0]
            name = treeItem["name"]
            treeItem.destroy ()
            try:
                os.removedirs (self.DBPath+"/"+name)
            except OSError:
                pass
    def searchCardItemInTree (self, tree, cardID):
        "Search in a  GTKTree for a GTKTreeItem with attribute name cardID"
        children = tree.children ()
        for child in children:
            if child["name"] == cardID: return child
            elif child["name"] == self.forceToCategory (child["name"]):
                # the child is a category node we can go down in the subtree
                found = self.searchCardItemInTree (child.subtree, cardID)
                if found != None: return found
        return None
    def pushCard (self,cardString):
        if cardString == None:
            return
        # Check if there is right now a conversion process
        if self.child_process != None:
            return
        self.selectedCard = card (cardString)
        
        self.widgets["IDEntry"].set_text (self.selectedCard.card["id"])
        self.widgets["level1Entry"].set_text (self.selectedCard.card["level1"])
        self.widgets["difficulty1Entry"].set_text (self.selectedCard.card["difficulty1"])
        self.widgets["level2Entry"].set_text (self.selectedCard.card["level2"])
        self.widgets["difficulty2Entry"].set_text (self.selectedCard.card["difficulty2"])
        self.widgets["level3Entry"].set_text (self.selectedCard.card["level3"])
        self.widgets["difficulty3Entry"].set_text (self.selectedCard.card["difficulty3"])
        self.widgets["keywordEntry"].set_text (self.selectedCard.card["keywords"])
        self.widgets["contributorEntry"].set_text (self.selectedCard.card["source"])
        self.widgets["emailEntry"].set_text (self.selectedCard.card["sourceEmail"])

        setBuffer (self.widgets["commentText"], self.selectedCard.card["comments"])
        setBuffer (self.widgets["latexText"], self.selectedCard.card["LaTeXHeader"])

        self.updateCardView ()
        self.tag = timeout_add(100, self.timeout)
        self.widgets["barre"].push ("Processing the preview...")

        # check if the card is marked as kept
        if self.keptCard.count (self.selectedCard.card["id"]):
            self.widgets["keepCard"].set_active (1)
        else:
            self.widgets["keepCard"].set_active (0)

    def fetchCard (self):
        newCard = card ()
        newCard.setCardItem ("id", self.widgets["IDEntry"].get_text ())
        newCard.setCardItem ("level1", self.widgets["level1Entry"].get_text ())
        newCard.setCardItem ("difficulty1", self.widgets["difficulty1Entry"].get_text ())
        newCard.setCardItem ("level2", self.widgets["level2Entry"].get_text ())
        newCard.setCardItem ("difficulty2", self.widgets["difficulty3Entry"].get_text ())
        newCard.setCardItem ("level3", self.widgets["level3Entry"].get_text ())
        newCard.setCardItem ("difficulty3", self.widgets["difficulty3Entry"].get_text ())
        newCard.setCardItem ("keywords", self.widgets["keywordEntry"].get_text ())
        newCard.setCardItem ("comments", self.widgets["commentText"].get_chars (0, -1))
        newCard.setCardItem ("LaTeXHeader", self.widgets["latexText"].get_chars (0, -1))
        newCard.setCardItem ("source", self.widgets["contributorEntry"].get_text ())
        newCard.setCardItem ("sourceEmail", self.widgets["emailEntry"].get_text ())
        return newCard
    
    def timeout(self, pcnt=[0]):
        pcnt[0] = (pcnt[0] + 2) % 100
        self.widgets["progress"].set_value (pcnt[0])

        if  self.child_process.poll() != -1: 
            timeout_remove (self.tag)
            self.widgets["pixmapPreview"].load_file (self.bitmapCard)
            self.widgets["barre"].pop ()
            del self.child_process
            self.child_process = None
            return FALSE        
        return TRUE        
    def buildTree (self, tree, path):
        directory = os.listdir (path)
        isNumber = re.compile ("^\\d+$")
        for file in directory:
            filep = os.path.join (path, file)
            if os.path.isdir (filep):
                item = GtkTreeItem (file)
                tree.append (item)
                item.show ()
                # item name is the category path (from the DB root)
                item["name"] = string.replace(filep, self.DBPath+"/", "")
                #we got a directory go down
                #check if this is a number in this case don't go down further
                #and set a callback to it
                if not isNumber.search (file):
                    subtree = GtkTree ()
                    item.set_subtree (subtree)
                    self.buildTree (subtree, filep)
                else:
                    item.connect ("select", self.cb_select_card, item["name"])

    def delTree (self):
        self.widgets["cardTree"].clear_items (0, -1)
