# Maxi Card
# Copyright (C) 2000 Free Software Foundation.
#
# Author: OFSET, Hilaire Fernandes
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the  Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
# USA.
#

from virtualClassUI import preferenceUI

from gtk import *
from gnome.ui import *
from GDK import *
from libglade import *

class preferenceGnome (preferenceUI):
    'Gnome implementation of the preference dialog'
    def __init__ (self, filename, parentUI, DBPath, database):
        preferenceUI.__init__ (self, parentUI)
        # load the widgets
        self.glade = GladeXML (filename, 'maxicardPropertyBox')
        # fetch some widget reference
        self.widgets = {
            'dialog' : self.glade.get_widget ('maxicardPropertyBox'),
            'databasePathEntry' : self.glade.get_widget ('databasePathEntry'),
            'databaseNameEntry' : self.glade.get_widget ('databaseNameEntry')
            }
        dic = {"on_maxicardPropertyBox_clicked" : self.dialogClicked,
            "on_maxicardPropertyBox_close" : self.dialogClosed,
            "on_maxicardPropertyBox_apply" : self.dialogApplied
            }
        self.glade.signal_autoconnect (dic)
        self.widgets['dialog'].button_connect (0,self.dialogClicked)

        # Feed the entries
        self.widgets['databasePathEntry'].set_text (DBPath)
        self.widgets['databaseNameEntry'].set_text (database)

    def dialogClicked (self, w):
        print 'hello'
        self.parentUI.initCardGnome (self.widgets['databasePathEntry'].get_text (),
                                     self.widgets['databaseNameEntry'].get_text ())
        
    def dialogClosed (self, w):
        pass
    
    def dialogApplied (self, w):
        pass
    
