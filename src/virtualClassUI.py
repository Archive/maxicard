# Maxi Card
# Copyright (C) 2000 Free Software Foundation.
#
# Author: OFSET, Hilaire Fernandes
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the  Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
# USA.
#

##################################################
#                                                # 
# Virtual UI class                               #
#                                                #
##################################################


import popen2, os, string, re
from card import card
from DBCard import DBCard
class cardUI:
    "Virtual class to handle the GUI"
    def __init__ (self, DBPath, database):
        # Possible value are {browseMode; browseSelectionMode;
        # findMode; insertMode; editMode}
        self.mode = None
        self.keptCard = []
        self.selectedCard = None
        self.DBPath = DBPath
        self.databaseName = database
        self.DataBase = DBCard (DBPath + "/" + database)
    def pushCard (self, cardString):
        "Push the card (a string) in the UI and create the card object"
        pass
    def fetchCard (self):
        "Return card object based on the content of the UI"
        pass
    def nextCard (self):
        "Push in the UI the next card of the database"
        pass
    def previousCard (self):
        "Push in the UI the previous card of the database"
        pass
    def keepCard (self, card):
        "Mark the card as kept"
        self.keptCard.append (card.card["id"])
    def unkeepCard (self, card):
        "Mark tha card as not kept"
        if self.keptCard.count (card.card["id"]):
            self.keptCard.remove (card.card["id"])
    def delCard (self):
        "Delete the card currently in the UI"
        pass
    def insertCard (self):
        "Insert in the database the card currently in the UI"
        pass
    def newCard (self):
        "Prepare the UI for a new card"
        if self.cardInUI != None:
            del self.cardInUI
        self.cardInUI = card.card ()
    def searchCard (self):
        "Prepare the UI to search a card"
        pass
    def updateCardView (self):
        "Check if the cached view of the card is up to date: \
        If date (LaTeX card) > date (bitmap view) \
        then update view. \
        Next return the relative path to the bitmap view from the root DB path"
        self.bitmapCard = self.DBPath + "/" + self.selectedCard.card["id"] +  "/card.jpg"
        self.latexCard = self.DBPath + "/" + self.selectedCard.card["id"] +  "/card.tex"
        # UNIX dependable code
        # The view is not up to date, update it

        cmd = '"./exo2jpg.sh" ' + '"'+self.DBPath+'" '+ \
              '"'+self.selectedCard.card["id"]+'" '+ \
              '"'+self.selectedCard.card["LaTeXHeader"]+'"'
        self.child_process = popen2.Popen3 (cmd)
        
    def forceToCategory (self, path):
        "Check if the path is a sub path to a card or category (ending by a number or not)\
        Then return the category path"
        lastFolder = string.rfind (path, "/")
        isNumber = re.compile ("^\\d+$")
        if isNumber.search (path[lastFolder + 1:]):
            return path[0:lastFolder - 1]
        else:
            return path

    def buildTree (self):
        "Build the UI tree from the database"
        pass
    def delTree (self):
        "Delete the UI tree"
        pass
        
class cardModeUI:
    "Specify the behavior of the UI - button, category \
    tree - according to the mode where is placed the UI."
    def ___init__ (self):
        "hide/show widgets..."
        pass
    def __del__ (self):
        "Disconnect the handlers"
        pass
    def nextPressed (self):
        "Action of the Next button"
        pass
    def previousPressed (self):
        "Action of the Previous button"
        pass
    def okPressed (self):
        "Action of the Ok button"
        pass
    def keepItPressed (self, active):
        "Action when keepit pressed"
        pass

class preferenceUI:
    "To manipulate the preferences UI"
    def __init__ (self, parentUI):
        self.parentUI = parentUI
    def __del__ (self):
        pass
