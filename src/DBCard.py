# Maxi Card
# Copyright (C) 2000 the Free Software Foundation.
#
# Author: Hilaire Fernandes
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the  Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
# USA.
#
import string
from card import card
class DBCard:    
    def __init__ (self, indexLocation):
        "Read the card index in memory"
        self.indexLocation = indexLocation
        self.position = 0
        # state of the database
        self.state = 'valid'
        #FIXME: catch the IOError if the file can't be opened
        try:
            file = open (indexLocation)
        except IOError:
            self.state = 'unvalid'
        else:
            self.cards = file.readlines ()
            file.close ()

    def __del__ (self):
        "Any cleaning?"
        pass
    def getAbsolutePath (self):
        "Return the absolute path to the root of the data base"
        return self.indexLocation
    def getCard (self, cardID):
        "Get string card from their ID - (first field in a record)"
        self.position = self.cardIndex (cardID)
        if self.position == -1: return None
        else: return self.readCardByIndex (self.position)
        
    def readCardByIndex (self, index):
        "Read string card at index position in the DB"
        if index < 0:
            return None
        for card in self.cards:
            if index == 0:
                return card
            index = index - 1
        return None
    def getNextCard (self):
        "Get the next string card"
        card = self.readCardByIndex (self.position + 1)
        if card != None:
            self.position = self.position + 1
        return card
    def getPreviousCard (self):
        "Get the previous string card"
        card = self.readCardByIndex (self.position - 1)
        if card != None:
            self.position = self.position - 1
        return card
    def getCurrentCard (self):
        "Get the string card with the current position index"
        return self.readCardByIndex (self.position)
    def updateCard (Self, card):
        "Update the card data in the database, card is an object representation of a card"
        pass
    def searchCard (self, searchDico):
        "The key is the category to search in, value can be preceded by - to negate the effect"
        return None
    def cardIndex (self, cardID):
        "Find a card given its ID and return its position in the data base or -1"
        position = 0
        find = -1
        card = self.readCardByIndex (position)
        while card != None:
            if string.find(card, cardID) != -1: return position
            position = position + 1
            card = self.readCardByIndex (position)
        return -1
    def appendCard (self, card):
        "Append the card in the database"
        self.cards.append (card.getStringCard ())
        file = open (self.indexLocation, "a")
        file.write (card.getStringCard () + "\n")
        file.close ()
    def delCard (self, card):
        index = self.cardIndex (card.card["id"])
        if index != -1:
            del self.cards[index]
            file = open (self.indexLocation, "w")
            file.writelines (self.cards)
            file.close ()
    def nextCard (self):
        pass
    def previousCard (self):
        pass
    

            
